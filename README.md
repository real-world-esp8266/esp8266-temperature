# ESP8266 Temperature

## Measure temp/humidity with DHT11 and send to hub

Uses ESP8266-hub, but can also be standalone.

### Purpose

Receives temperature and humidity information from a connected DHT11, formats that information for statsd consumption, then sends the data over UDP.

### Setup

1. Connect necessary hardware
2. `git clone`
3. Obtain necessary dependencies ([Adafruit Unified Sensor Driver](https://github.com/adafruit/Adafruit_Sensor) and [Adafruit DHT sensor library](https://github.com/adafruit/DHT-sensor-library))
3. Replace necessary variables
4. Upload and provide power :)

### Testing

1. Connect to network
2. See if metrics are being sent/received (the hub makes this easy by checking the serial monitor output)

### In Progress
