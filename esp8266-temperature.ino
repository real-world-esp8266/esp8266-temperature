#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "DHT.h"

#define DHTPIN D1
#define DHTTYPE DHT11

#define FLASH 0
#define HUB 1

const char ssid[] = "SoftNetworkSSID";
const char password[] = "SoftNetworkPassword";
const char ip[] = "192.168.4.1"; // this may be either the hub IP or the statsd server IP
unsigned int port = 3000; // this is either the hub UDP port or the stastd server UDP port
WiFiUDP udp;

const char location[] = "which_room";

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  if (FLASH) {
    pinMode(LED_BUILTIN, OUTPUT);
  }

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    ESP.restart();
  }
  udp.begin(port);

  dht.begin();

  ArduinoOTA.onStart([]() {
    flash(1);
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    flash(1);
  });

  ArduinoOTA.onEnd([]() {
    flash(5);
  });

  ArduinoOTA.onError([](ota_error_t error) {
    if (error == OTA_AUTH_ERROR) {
      flash(1);
    } else if (error == OTA_BEGIN_ERROR) {
      flash(1);
    } else if (error == OTA_CONNECT_ERROR) {
      flash(1);
    } else if (error == OTA_RECEIVE_ERROR) {
      flash(1);
    } else if (error == OTA_END_ERROR) {
      flash(1);
    }
  });

  ArduinoOTA.begin();
}

void loop() {
  ArduinoOTA.handle();
  getHumidity();
  getTemperature();
  if (HUB) {
    sendSelf();
  }
  delay(1000);
}

void flash(unsigned int count) {
  if (FLASH) {
    for (int i = 0; i < count; i++) {
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
    }
  }
}

void sendSelf() {
  if (FLASH) {
    digitalWrite(LED_BUILTIN, LOW);
  }

  udp.beginPacket(ip, port);
  char info[100];
  sprintf(info, "hub:%s:%s", location, WiFi.localIP().toString().c_str());
  udp.write(info);
  udp.endPacket();

  if (FLASH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

void getHumidity() {
  if (FLASH) {
    digitalWrite(LED_BUILTIN, LOW);
  }

  float h = dht.readHumidity();
  if (isnan(h)) {
    return;
  }

  udp.beginPacket(ip, port);
  char info[100];
  sprintf(info, "humidity._t_location.%s:%f|c", location, h);
  udp.write(info);
  udp.endPacket();

  if (FLASH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

void getTemperature() {
  if (FLASH) {
    digitalWrite(LED_BUILTIN, LOW);
  }

  float t = dht.readTemperature();
  if (isnan(t)) {
    return;
  }

  udp.beginPacket(ip, port);
  char info[100];
  sprintf(info, "temperature._t_location.%s:%f|c", location, t);
  udp.write(info);
  udp.endPacket();

  if (FLASH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}
